package com.example.core.gateway

import com.example.core.model.DocumentModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * @author Denis Kleshchev
 */
interface DocumentsGateway {

    fun listenForEmployee(id: Int): Flowable<List<DocumentModel>>

    fun getForEmployee(id: Int): Single<List<DocumentModel>>

    fun createRandomForEmployee(id: Int): Completable

    fun removeDocument(document: DocumentModel): Completable

    fun removeAllFor(id: Int): Completable
}