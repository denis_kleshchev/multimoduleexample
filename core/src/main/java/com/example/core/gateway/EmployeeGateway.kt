package com.example.core.gateway

import com.example.core.model.EmployeeModel
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * @author Denis Kleshchev
 */
interface EmployeeGateway {

    val listen: Flowable<List<EmployeeModel>>

    fun createRandom(): Completable

    fun remove(id: Int): Completable
}