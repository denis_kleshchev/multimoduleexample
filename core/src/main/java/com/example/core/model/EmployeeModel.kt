package com.example.core.model

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

/**
 * @author Denis Kleshchev
 */
data class EmployeeModel(
    val id: Int,
    val firstName: String,
    val lastName: String,
    val docsAmount: Int
) : ViewModel
