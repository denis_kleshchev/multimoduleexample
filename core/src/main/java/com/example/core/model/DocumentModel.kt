package com.example.core.model

import com.github.vivchar.rendererrecyclerviewadapter.ViewModel

/**
 * @author Denis Kleshchev
 */
data class DocumentModel(
    val id: Int,
    val employeeId: Int,
    val type: String,
    val content: String
) : ViewModel
