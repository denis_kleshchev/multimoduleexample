package com.example.core

import io.reactivex.Completable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * @author Denis Kleshchev
 */
fun Disposable?.safeDispose() {
    this?.apply {
        if (!isDisposed) {
            dispose()
        }
    }
}

fun Completable.subscribeOnIo() = subscribeOn(Schedulers.io())