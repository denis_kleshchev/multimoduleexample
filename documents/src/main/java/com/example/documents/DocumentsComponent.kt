package com.example.documents

import com.example.common.di.AppComponent
import com.example.common.di.scopes.DocumentsScope
import dagger.Component

/**
 * @author Denis Kleshchev
 */
@DocumentsScope
@Component(
    dependencies = [AppComponent::class]
)
interface DocumentsComponent {

    fun inject(documentsActivity: DocumentsActivity)
}