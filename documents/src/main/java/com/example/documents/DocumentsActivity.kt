package com.example.documents

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.common.di.Injector
import com.example.core.gateway.DocumentsGateway
import com.example.core.model.DocumentModel
import com.example.core.safeDispose
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_documents.*
import javax.inject.Inject

class DocumentsActivity : AppCompatActivity() {

    @Inject
    lateinit var repository: DocumentsGateway

    private lateinit var documentsComponent: DocumentsComponent

    private var repositoryDisposable: Disposable? = null
    private var subscriptions: CompositeDisposable? = null

    private val employeeId: Int by lazy {
        intent.extras?.getInt(EMPLOYEE_ID_KEY) ?: throw RuntimeException("no employee id passed!")
    }

    private val adapter = RendererRecyclerViewAdapter().apply {
        registerRenderer(
            ViewBinder(R.layout.item_document, DocumentModel::class.java) { model, finder, _ ->
                finder.setText(R.id.typeText, model.type)
                finder.setText(R.id.contentText, model.content)
                finder.setOnClickListener { onDocumentClick(model) }
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        subscriptions = CompositeDisposable()

        intent.extras?.getString(EMPLOYEE_NAME_KEY)?.let {
            supportActionBar?.title = it
        }

        setContentView(R.layout.activity_documents)
        documentsList.adapter = adapter
        fab.setOnClickListener { addDocument() }
    }

    override fun onStart() {
        super.onStart()
        listenDocuments()
    }

    override fun onStop() {
        super.onStop()
        repositoryDisposable.safeDispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        repositoryDisposable.safeDispose()
        subscriptions.safeDispose()
    }

    private fun inject() {
        documentsComponent = DaggerDocumentsComponent.builder().appComponent(Injector.appComponent).build()
        documentsComponent.inject(this)
    }

    private fun listenDocuments() {
        repositoryDisposable = repository.listenForEmployee(employeeId)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    handleList(it)
                },
                {
                    Log.e("ERROR::", "error occurred: $it")
                }
            )
    }

    private fun handleList(documents: List<DocumentModel>) {
        emptyView.isVisible = documents.isEmpty()
        documentsList.isVisible = documents.isNotEmpty()
        adapter.setItems(documents)
        adapter.notifyDataSetChanged()
    }

    private fun addDocument() {
        subscriptions?.add(repository.createRandomForEmployee(employeeId).subscribe())
    }

    private fun onDocumentClick(document: DocumentModel) {
        subscriptions?.add(repository.removeDocument(document).subscribe())
    }

    companion object {

        private const val EMPLOYEE_ID_KEY = "employee_id_key"
        private const val EMPLOYEE_NAME_KEY = "employee_name_key"

        @JvmStatic
        fun createInstance(context: Context, employeeId: Int, employeeName: String): Intent =
            Intent(context, DocumentsActivity::class.java).apply {
                putExtra(EMPLOYEE_ID_KEY, employeeId)
                putExtra(EMPLOYEE_NAME_KEY, employeeName)
            }
    }
}
