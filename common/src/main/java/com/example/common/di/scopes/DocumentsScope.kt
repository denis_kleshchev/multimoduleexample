package com.example.common.di.scopes

import javax.inject.Scope

/**
 * @author Denis Kleshchev
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class DocumentsScope
