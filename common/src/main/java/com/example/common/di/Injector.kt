package com.example.common.di

import android.content.Context
import com.example.common.di.modules.ContextModule

/**
 * @author Denis Kleshchev
 */
object Injector {

    lateinit var appComponent: AppComponent

    fun initAppComponent(context: Context) {
        appComponent = DaggerAppComponent.builder()
            .contextModule(ContextModule(context))
            .build()
    }
}