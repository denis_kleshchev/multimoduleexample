package com.example.common.di.modules

import com.example.common.di.scopes.AppScope
import com.example.core.gateway.DocumentsGateway
import com.example.core.gateway.EmployeeGateway
import com.example.db.dao.DocumentDao
import com.example.db.dao.EmployeeDao
import com.example.db.repository.DocumentRepository
import com.example.db.repository.EmployeeRepository
import dagger.Module
import dagger.Provides

/**
 * @author Denis Kleshchev
 */
@Module
class GatewayModule {

    @AppScope
    @Provides
    fun providesDocumentsGateway(documentDao: DocumentDao): DocumentsGateway {
        return DocumentRepository(documentDao)
    }

    @AppScope
    @Provides
    fun providesEmployeeGateway(employeeDao: EmployeeDao) : EmployeeGateway {
        return EmployeeRepository(employeeDao)
    }
}