package com.example.common.di.modules

import android.content.Context
import androidx.room.Room
import com.example.common.di.scopes.AppScope
import com.example.db.AppDatabase
import com.example.db.dao.DocumentDao
import com.example.db.dao.EmployeeDao
import dagger.Module
import dagger.Provides

/**
 * @author Denis Kleshchev
 */
@Module
class DbModule {

    @AppScope
    @Provides
    fun providesDb(context: Context): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            DATABASE_NAME
        )
            .build()
    }

    @AppScope
    @Provides
    fun providesEmployeeDao(database: AppDatabase): EmployeeDao {
        return database.employeeDao()
    }

    @AppScope
    @Provides
    fun providesDocumentDao(database: AppDatabase): DocumentDao {
        return database.documentDao()
    }

    companion object {
        private const val DATABASE_NAME = "multi_module_example_database"
    }
}