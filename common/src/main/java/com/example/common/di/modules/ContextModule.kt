package com.example.common.di.modules

import android.content.Context
import com.example.common.di.scopes.AppScope
import dagger.Module
import dagger.Provides

/**
 * @author Denis Kleshchev
 */
@Module
class ContextModule(private val context: Context) {

    @AppScope
    @Provides
    fun providesContext(): Context = context
}