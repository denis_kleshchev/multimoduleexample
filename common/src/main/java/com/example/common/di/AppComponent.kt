package com.example.common.di

import android.app.Application
import com.example.common.di.modules.ContextModule
import com.example.common.di.modules.DbModule
import com.example.common.di.modules.GatewayModule
import com.example.common.di.scopes.AppScope
import com.example.core.gateway.DocumentsGateway
import com.example.core.gateway.EmployeeGateway
import dagger.Component

/**
 * @author Denis Kleshchev
 */
@AppScope
@Component(
    modules = [
        ContextModule::class,
        DbModule::class,
        GatewayModule::class
    ]
)
interface AppComponent {

    fun inject(application: Application)

    fun providesDocumentsGateway(): DocumentsGateway

    fun providesEmployeeGateway(): EmployeeGateway
}