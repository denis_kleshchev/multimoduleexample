package com.example.multiapp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import com.example.common.di.Injector
import com.example.core.model.EmployeeModel
import com.example.core.safeDispose
import com.example.documents.DocumentsActivity
import com.github.vivchar.rendererrecyclerviewadapter.RendererRecyclerViewAdapter
import com.github.vivchar.rendererrecyclerviewadapter.binder.ViewBinder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var interactor: EmployeeInteractor

    private lateinit var component: MainComponent

    private var employeesDisposable: Disposable? = null
    private var subscriptions: CompositeDisposable? = null

    private val adapter = RendererRecyclerViewAdapter().apply {
        registerRenderer(
            ViewBinder(R.layout.item_employee, EmployeeModel::class.java) { model, finder, _ ->
                finder.setText(R.id.firstNameView, model.firstName)
                finder.setText(R.id.lastNameView, model.lastName)
                finder.setText(R.id.docsAmountView, "Документов: ${model.docsAmount}")
                finder.setOnClickListener { selectEmployee(model) }
                finder.setOnLongClickListener(R.id.rootView) {
                    subscriptions?.add(interactor.delete(model).subscribe())
                    true
                }
            }
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()

        subscriptions = CompositeDisposable()

        setContentView(R.layout.activity_main)
        employeesList.adapter = adapter
        fab.setOnClickListener { addEmployee() }
    }

    override fun onStart() {
        super.onStart()
        listenEmployees()
    }

    override fun onStop() {
        super.onStop()
        employeesDisposable.safeDispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        employeesDisposable.safeDispose()
        subscriptions.safeDispose()
    }

    private fun inject() {
        component = DaggerMainComponent.builder()
            .appComponent(Injector.appComponent)
            .build()
        component.inject(this)
    }

    private fun listenEmployees() {
        employeesDisposable = interactor.listen
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                {
                    handleList(it)
                },
                {
                    Log.e("ERROR::", "error occurred: $it")
                }
            )
    }

    private fun handleList(employees: List<EmployeeModel>) {
        emptyView.isVisible = employees.isEmpty()
        employeesList.isVisible = employees.isNotEmpty()
        adapter.setItems(employees)
        adapter.notifyDataSetChanged()
    }

    private fun addEmployee() {
        subscriptions?.add(interactor.createRandom().subscribe())
    }

    private fun selectEmployee(employee: EmployeeModel) {
        startActivity(DocumentsActivity.createInstance(this, employee.id, employee.firstName))
    }
}
