package com.example.multiapp

import android.content.Context
import androidx.multidex.MultiDexApplication
import com.example.common.di.Injector
import javax.inject.Inject

/**
 * @author Denis Kleshchev
 */
class MultiModuleApplication : MultiDexApplication() {

    @Inject
    lateinit var context: Context

    override fun onCreate() {
        super.onCreate()
        Injector.initAppComponent(this)
        Injector.appComponent.inject(this)
    }
}