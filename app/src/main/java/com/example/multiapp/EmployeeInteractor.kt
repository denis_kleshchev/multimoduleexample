package com.example.multiapp

import com.example.core.gateway.DocumentsGateway
import com.example.core.gateway.EmployeeGateway
import com.example.core.model.EmployeeModel
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import javax.inject.Inject

/**
 * @author Denis Kleshchev
 */
class EmployeeInteractor @Inject constructor(
    private val employeeGateway: EmployeeGateway,
    private val documentsGateway: DocumentsGateway
) {

    val listen: Flowable<List<EmployeeModel>>
        get() = employeeGateway.listen
            .flatMapSingle { employees ->
                Observable.fromIterable(employees)
                    .flatMapSingle { employee ->
                        documentsGateway.getForEmployee(employee.id)
                            .map { employee.copy(docsAmount = it.size) }
                    }
                    .toList()
            }

    fun createRandom(): Completable {
        return employeeGateway.createRandom()
    }

    fun delete(employee: EmployeeModel): Completable {
        return documentsGateway.removeAllFor(employee.id)
            .andThen(employeeGateway.remove(employee.id))
    }
}