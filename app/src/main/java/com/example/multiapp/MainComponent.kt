package com.example.multiapp

import com.example.common.di.AppComponent
import com.example.common.di.scopes.MainScope
import dagger.Component

/**
 * @author Denis Kleshchev
 */
@MainScope
@Component(
    dependencies = [AppComponent::class]
)
interface MainComponent {

    fun inject(activity: MainActivity)
}