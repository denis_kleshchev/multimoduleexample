package com.example.db.converter

import com.example.core.model.DocumentModel
import com.example.db.entities.Document
import javax.inject.Inject

/**
 * @author Denis Kleshchev
 */
class DocumentConverter @Inject constructor(

) : Converter<Document, DocumentModel> {

    override fun toBusiness(dto: Document): DocumentModel {
        return with(dto) {
            DocumentModel(id, employeeId, type, content)
        }
    }

    override fun toDto(business: DocumentModel): Document {
        return with(business) {
            Document(id, employeeId, type, content)
        }
    }
}