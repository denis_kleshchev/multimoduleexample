package com.example.db.converter

/**
 * @author Denis Kleshchev
 */
interface Converter<T, R> {

    fun toBusiness(dto: T): R

    fun toDto(business: R): T
}