package com.example.db.converter

import com.example.core.model.EmployeeModel
import com.example.db.entities.Employee
import javax.inject.Inject

/**
 * @author Denis Kleshchev
 */
class EmployeeConverter @Inject constructor(

) : Converter<Employee, EmployeeModel> {

    fun toBusiness(dto: Employee, docsAmount: Int): EmployeeModel {
        return with(dto) {
            EmployeeModel(id, firstName, lastName, docsAmount)
        }
    }

    override fun toBusiness(dto: Employee): EmployeeModel {
        return with(dto) {
            EmployeeModel(id, firstName, lastName, 0)
        }
    }

    override fun toDto(business: EmployeeModel): Employee {
        return with(business) {
            Employee(id, firstName, lastName)
        }
    }
}