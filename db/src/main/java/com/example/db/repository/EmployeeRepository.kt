package com.example.db.repository

import com.example.core.gateway.EmployeeGateway
import com.example.core.model.EmployeeModel
import com.example.core.subscribeOnIo
import com.example.db.converter.Converter
import com.example.db.converter.EmployeeConverter
import com.example.db.dao.EmployeeDao
import com.example.db.entities.Employee
import io.reactivex.Completable
import io.reactivex.Flowable
import kotlin.random.Random

/**
 * @author Denis Kleshchev
 */
class EmployeeRepository(
    private val employeeDao: EmployeeDao
) : EmployeeGateway {

    private val converter: Converter<Employee, EmployeeModel> = EmployeeConverter()

    override val listen: Flowable<List<EmployeeModel>>
        get() = employeeDao.listen()
            .map { list ->
                list.map { converter.toBusiness(it) }
            }

    override fun createRandom(): Completable {
        return employeeDao.insert(createRandomEmployee()).subscribeOnIo()
    }

    override fun remove(id: Int): Completable {
        return employeeDao.delete(id).subscribeOnIo()
    }

    private fun createRandomEmployee(): Employee {
        return Employee(
            0,
            "Tirion ${Random.nextInt()}",
            "Lannister ${Random.nextInt()}"
        )
    }
}
