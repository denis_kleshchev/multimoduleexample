package com.example.db.repository

import com.example.core.gateway.DocumentsGateway
import com.example.core.model.DocumentModel
import com.example.core.subscribeOnIo
import com.example.db.converter.Converter
import com.example.db.converter.DocumentConverter
import com.example.db.dao.DocumentDao
import com.example.db.entities.Document
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import kotlin.random.Random

/**
 * @author Denis Kleshchev
 */
class DocumentRepository(
    private val documentDao: DocumentDao
) : DocumentsGateway {

    private val converter: Converter<Document, DocumentModel> = DocumentConverter()

    override fun listenForEmployee(id: Int): Flowable<List<DocumentModel>> {
        return documentDao.listenAllForUser(id)
            .map { list -> list.map { converter.toBusiness(it) } }
    }

    override fun getForEmployee(id: Int): Single<List<DocumentModel>> {
        return documentDao.getAllForUser(id)
            .map { list -> list.map { converter.toBusiness(it) } }
    }

    override fun createRandomForEmployee(id: Int): Completable {
        return documentDao.insert(generateDocumentFor(id)).subscribeOnIo()
    }

    override fun removeDocument(document: DocumentModel): Completable {
        return documentDao.delete(converter.toDto(document)).subscribeOnIo()
    }

    override fun removeAllFor(id: Int): Completable {
        return documentDao.deleteAllForUser(id).subscribeOnIo()
    }

    private fun generateDocumentFor(id: Int): Document {
        return Document(
            0,
            id,
            types[Random.nextInt(0, 3)],
            "Серия ${Random.nextInt()} номер ${Random.nextInt()}"
        )
    }

    companion object {
        private val types = listOf("ВУ", "Паспорт", "СНИЛС")
    }
}