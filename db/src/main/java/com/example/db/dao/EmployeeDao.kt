package com.example.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.db.entities.Employee
import io.reactivex.Completable
import io.reactivex.Flowable

/**
 * @author Denis Kleshchev
 */
@Dao
interface EmployeeDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(employee: Employee): Completable

    @Query("SELECT * FROM employees")
    fun listen(): Flowable<List<Employee>>

    @Query("DELETE FROM employees WHERE id = :employeeId")
    fun delete(employeeId: Int): Completable

}
