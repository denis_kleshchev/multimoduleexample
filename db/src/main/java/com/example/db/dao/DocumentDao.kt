package com.example.db.dao

import androidx.room.*
import com.example.db.entities.Document
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * @author Denis Kleshchev
 */
@Dao
interface DocumentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(document: Document): Completable

    @Query("SELECT * FROM documents WHERE employee_id = :employeeId")
    fun getAllForUser(employeeId: Int): Single<List<Document>>

    @Query("SELECT * FROM documents WHERE employee_id = :employeeId")
    fun listenAllForUser(employeeId: Int): Flowable<List<Document>>

    @Delete
    fun delete(document: Document): Completable

    @Query("DELETE FROM documents WHERE employee_id = :employeeId")
    fun deleteAllForUser(employeeId: Int): Completable

}
