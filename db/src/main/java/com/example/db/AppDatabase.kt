package com.example.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.db.dao.DocumentDao
import com.example.db.dao.EmployeeDao
import com.example.db.entities.Document
import com.example.db.entities.Employee

/**
 * @author Denis Kleshchev
 */
@Database(entities = [Employee::class, Document::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun employeeDao(): EmployeeDao

    abstract fun documentDao(): DocumentDao

}
